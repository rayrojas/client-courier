from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'daphne.views.index'),
	url(r'^login/$', 'daphne.views.login'),
	url(r'^logout/$', 'daphne.views.onlogout'),
	url(r'^tools/search-document/$', 'daphne.views.tools_search_document'),
)
