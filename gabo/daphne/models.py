#encoding:utf-8
from django.db import models
from django.contrib.auth.models import User, UserManager, Group
from django.utils.encoding import smart_str, smart_unicode

# Create your models here.
class Usuario(User):
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", null = False, blank = False)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móvil.", null = False, blank = False)	
	objects = UserManager()
	def __unicode__(self):
		return  u'%s' % (self.username + " - " + self.last_name + " " + self.first_name)

	def get_Grupos(self):
		grupos = ""
		for x in self.groups.all():
			grupos += ", "+str(x.name)
		return u'%s' % (grupos[2:len(grupos)])

	def get_Pseudo(self):
		return (self.first_name[0] + self.last_name.split(" ")[0])

	class Meta:
		app_label = 'usuario'

class ClienteEmpresaLocal(models.Model):
	local = models.CharField(max_length = 250, verbose_name = 'Nombre del local', help_text = "Ingrese el nombre del local", default='No definido', null = False, blank = False)
	responsable = models.CharField(max_length = 250, verbose_name = 'Nombre del responsable', help_text = "Ingrese el nombre del responsable", null = False, blank = False)
	direccion = models.CharField(max_length = 250, verbose_name = "Domicilio", help_text = "Ingrese el domicilio.", blank = True, null = False )
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", blank = True, null = False)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móbil.", blank = True, null = False)
	rpm = models.CharField(max_length = 15, verbose_name = "Teléfono RPM/RPC", help_text = "Ingrese el número RPM/RPC.", blank = True, null = False)
	email = models.EmailField(verbose_name = "Correo Eléctronico", help_text = "Ingrese el correo eléctronico", blank = True, null = False)		

	def __unicode__(self):
		return u'%s' % (self.local + " - " + self.responsable)	

	class Meta:
		app_label = 'courier'

class Encomienda(models.Model):
	remitoof = models.ForeignKey('self', blank=True, null=True)
	isremito = models.BooleanField(verbose_name = "¿Es remito?", help_text = "¿Marque si es remito?", null = False, blank = False)
	cargo = models.PositiveIntegerField(verbose_name = "Cargo", help_text = "Ingrese el número de Cargo.", null = False, blank = False)
	estado = models.CharField(max_length = 3, verbose_name = "Estado", help_text = "Seleccione el estado.", default = "ENV", blank = False, null = False) #Pendiente, Salida, Entregado, Cargo, Cancelado, Perdido
	ndocumento = models.CharField(max_length=250, verbose_name = "N° de documento", help_text = "Ingrese el número de documento.", null = False, blank = False)
	nfactura = models.CharField(max_length=250, verbose_name = "N° de factura", help_text = "Ingrese el número de factura.", default="Ninguno", null = False, blank = False)
	fecha = models.DateTimeField(verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	def __unicode__(self):
		return u'%s' % (str(self.cargo))
		
	class Meta:
		app_label = 'courier'

class Encomienda_Juridico(models.Model):
	Encomienda =  models.ForeignKey(Encomienda, verbose_name = "Seleccione una Encomienda.")	
	transaccion = models.CharField(max_length = 1, verbose_name = "Transaccion", blank = False, null = False) # Envia o recibe
	Persona = models.ForeignKey(ClienteEmpresaLocal)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")
	def __unicode__(self):
		return u'%s' % (str(self.Encomienda) + " - " + str(self.transaccion))

	class Meta:
		app_label = 'courier'

class ClienteELMonitor(models.Model): # cliente empresa local monitor
	Local = models.ForeignKey(ClienteEmpresaLocal, verbose_name = "Seleccione el Cliente Empresa Local.")
	Monitor = models.ForeignKey(Usuario, related_name='User_for_Monitor', verbose_name = "monitor")
	apikey = models.CharField(max_length=250, editable=False, verbose_name = "apikey", help_text = "Ingrese el contenido.", default="No especificado", null = False, blank = False)
	oauth = models.CharField(max_length=250, editable=False, verbose_name = "oauth", help_text = "Ingrese el contenido.", default="No especificado", null = False, blank = False)
	creacion = models.DateTimeField(auto_now_add = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)
	modificacion = models.DateTimeField(auto_now = True, verbose_name="Fecha de Ingreso.", help_text = "Ingrese la Fecha de Ingreso.", null = False, blank = False)	
	usuario = models.ForeignKey(Usuario, verbose_name = "Usuario")

	def __unicode__(self):
		return u'%s' % (smart_str(self.Local) + " " + smart_str(self.Monitor))

	class Meta:
		app_label = 'courier'