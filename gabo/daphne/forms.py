#encoding: utf-8
from django import forms
from django.db import models

class LoginForm(forms.Form):
	username = forms.CharField(label = "Usuario", max_length=30)
	password = forms.CharField(label = "Contraseña", widget = forms.PasswordInput)
	
