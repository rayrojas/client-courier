#encoding:utf-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.template import RequestContext
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect, HttpResponse
from daphne.forms import LoginForm
from daphne.models import Usuario, Encomienda, Encomienda_Juridico, ClienteELMonitor

from datetime import datetime
from sislog import logear

import json
import traceback
import sys

def getAplicacion():
	return {
		'app' : {'nombre' : 'Sistema de Información'}
	}

def isLogin(u):
	if u is not None and u.is_active:
		return True
	else:
		return False

def hasPermission(u, patron):
	for uu in u:
		if uu.id == patron:
			return True
	return False
	
def index(request):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	try:
		usuario = Usuario.objects.get(id = usuario.id)
	except Exception, e:
		messages.error(request, "No se encontró un usuario con código: " + str(usuario.id))
		error = traceback.extract_stack()[-1]
		logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
		return HttpResponseRedirect("/404/")
	context = {
		'aplicacion' : getAplicacion(),
		'usuario' : usuario,
	}
	try:
		context['local'] = ClienteELMonitor.objects.filter(Monitor = usuario.id)[0]
	except Exception, e:
		messages.error(request, "No se encontró un local para este Monitor")
		error = traceback.extract_stack()[-1]
		logear(str(datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
		return HttpResponseRedirect("/404/")

	return render_to_response('index.html', context, context_instance = RequestContext(request))


def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			passwd = form.cleaned_data['password']
			user = authenticate(username = username, password = passwd)
			if user is not None and hasPermission(user.groups.all(), 6):
				if user.is_active:
					auth_login(request, user)
					return HttpResponseRedirect('/')
				else:
					context = {
						'formlogin' : form,
					}
			else:
				context = {
					'formlogin' : form,
				}
		else:
			form = LoginForm()
			context = {
				'formlogin' : form,
			}
	else:
		form = LoginForm()
		context = {
			'formlogin' : form,
		}
					
	return render_to_response('login.html', context, context_instance = RequestContext(request))

def tools_search_document(request):
	if request.method == 'GET':
		doReturnText = []
		doReturnVal = []		
		if "query" in request.GET and "local" in request.GET:
			
			for x in Encomienda_Juridico.objects.filter(Persona=request.GET['local'], Encomienda__ndocumento__istartswith = request.GET['query'], transaccion="E").order_by('Encomienda__fecha'):
				doReturnText.append(x.Encomienda.ndocumento + " / " + str(x.Encomienda.fecha.strftime("%d-%m-%Y")))
				doReturnVal.append(x.Encomienda.id)
			doReturn = {
				'query' : request.GET['query'],
				'suggestions' : doReturnText,
				'data' : doReturnVal
			}
			return HttpResponse(simplejson.dumps(doReturn), mimetype='application/json')
	return HttpResponse(status=400)

def onlogout(request):
	logout(request)
	return HttpResponseRedirect('/')