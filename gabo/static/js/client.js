$(document).on("ready", function(){
	var options = { 'serviceUrl' : '/tools/search-document/?local=' + generals['local'],
		minChars: 3,
		delimiter: /(,|;)\s*/,
		onSelect: function(value, data){
  			$.getJSON(ROOT+'/bk/core/encomienda/?format=jsonp&id=' + data + "&callback=?", function(request){
  				var objects = request['objects'][0];
  				$("#text_ndocumento").text(objects['ndocumento']);
  				$("#text_estado").text(objects['estadotext']);
  			});
		}
	};
	var query = $("#query_ndocumento").autocomplete(options);
});